# Website - Frontend

This is the frontend of my personal Website.It is published for educational purposes and for the sake of transparency.

## Installation

Download it via git and then start the [nuxt.js](https://nuxtjs.org) server.

```sh
git clone https://github.com/LeeDJD/Website-Frontend.git .
npm install
npm run build
npm run start
```

## Contributing

 Pull requests are welcome.For major changes, please open an issue and state your changes.

 Please make sure the Tests are passing.

## License

This project is licensed under the [MIT](https://choosealicense.com/licenses/mit) license.